#!/usr/bin/env python

import os
import sys
import subprocess
import threading
import Queue
import time
import random
import datetime
import sqlite3
import math
import traceback
import argparse
import signal
from collections import OrderedDict
from collections import Counter

DB_FILENAME = 'runr_db.sqlite'

def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')
    
    init_parser = subparsers.add_parser('init')
    
    add_parser = subparsers.add_parser('add')
    add_parser.add_argument('exec_path', metavar='<executable>')
    add_parser.add_argument('working_dir', metavar='<working-directory>', nargs='*')
    
    remove_parser = subparsers.add_parser('remove')
    remove_parser.add_argument('--force', '-f', action='store_true')
    remove_parser.add_argument('dir', metavar='<root-directory>', nargs='*')
    
    cancel_parser = subparsers.add_parser('cancel')
    cancel_parser.add_argument('dir', metavar='<root-directory>', nargs='*')
    
    status_parser = subparsers.add_parser('status')
    status_parser.add_argument('dir', metavar='<root-directory>', nargs='*')
    status_parser.add_argument('--state', '-s', '-S', metavar='<state>', default='all')
    
    summary_parser = subparsers.add_parser('summary')
    summary_parser.add_argument('dir', metavar='<root-directory>', nargs='*')
    summary_parser.add_argument('--state', '-s', '-S', metavar='<state>', default='all')
    
    list_parser = subparsers.add_parser('list')
    list_parser.add_argument('dir', metavar='<root-directory>', nargs='*')
    list_parser.add_argument('--state', '-s', '-S', metavar='<state>', default='all')
    
    go_parser = subparsers.add_parser('go')
    go_parser.add_argument('processes', metavar='<parallel-processes>', type=int, default=1)
    
    reset_parser = subparsers.add_parser('reset')
    reset_parser.add_argument('--state', '-s', '-S', metavar='<state>', default='all')
    reset_parser.add_argument('--force', action='store_true')

    args = parser.parse_args()
    
    if args.command == 'init':
        runr_init(args)
    elif args.command == 'add':
        runr_add(args)
    elif args.command == 'remove':
        runr_remove(args)
    elif args.command == 'cancel':
        runr_cancel(args)
    elif args.command == 'status':
        runr_status(args, mode = 'status')
    elif args.command == 'summary':
        runr_status(args, mode = 'summary')
    elif args.command == 'list':
        runr_status(args, mode = 'list')
    elif args.command == 'go':
        runr_go(args)
    elif args.command == 'reset':
        runr_reset(args)

def db_connect_exclusive(db_filename):
    connected = False
    while not connected:
        try:
            db = sqlite3.connect(db_filename, timeout = 60, isolation_level = None)
            db.row_factory = sqlite3.Row
            db.execute('BEGIN EXCLUSIVE')
            connected = True
        except Exception as e:
            sys.stderr.write('An error occurred accessing the database:\n')
            traceback.print_exc()
            sys.stderr.write('Retrying after ~ 1-second delay...\n')
            time.sleep(random.expovariate(1.0))
    
    return db

def db_disconnect(db):
    try:
        db.execute('COMMIT')
        db.close()
    except:
        pass

def runr_init(args):
    if os.path.exists(DB_FILENAME):
        sys.stderr.write('Found existing {}. Cannot init.\n'.format(DB_FILENAME))
        sys.exit(1)
    
    db = db_connect_exclusive(DB_FILENAME)
    
    db.execute('''CREATE TABLE jobs (
        working_dir TEXT, exec_path TEXT, creation_time TEXT, state TEXT, worker_id INTEGER, start_time TEXT, end_time TEXT, status_code INTEGER
    );''')
    db.execute('''CREATE INDEX jobs_index ON jobs (
        working_dir, exec_path, creation_time, state
    );''')
    
    db.execute('''CREATE TABLE workers (
        worker_id INTEGER, processes INTEGER, start_time TEXT, end_time TEXT
    );''')
    
    db.execute('''CREATE TABLE worker_messages (
        worker_id INTEGER, time TEXT, command TEXT, working_dir TEXT
    );''')
    db.execute('CREATE INDEX worker_messages_index ON workers (worker_id)')
    
    db_disconnect(db)

def runr_add(args):
    db_filename = identify_db_filename()
    if db_filename is None:
        sys.stderr.write('No runr database found. Cannot add.\n')
        sys.exit(1)
    
    if os.path.isdir(args.exec_path):
        sys.stderr.write('Executable path {} is a directory. Cannot use as executable.\n'.format(args.exec_path))
        sys.exit(1)
    if not os.path.isfile(args.exec_path):
        sys.stderr.write('Executable path {} does not exist. Cannot add.\n'.format(args.exec_path))
        sys.exit(1)
    if not os.access(args.exec_path, os.X_OK):
        sys.stderr.write('Executable path {} is not executable. Cannot use.\n'.format(args.exec_path))
        sys.exit(1)
    if os.path.isabs(args.exec_path):
        exec_path = args.exec_path
    else:
        exec_path = relative_path(args.exec_path, db_filename, subpath_required = False)
    
    for working_dir_raw in args.working_dir:
        if not os.path.exists(working_dir_raw):
            sys.stderr.write('Working directory {} does not exist. Cannot add.\n'.format(working_dir_raw))
            continue
        working_dir = relative_path(working_dir_raw, db_filename, subpath_required = True)
        if working_dir is None:
            sys.stderr.write('Working directory {} is not a subpath of the root. Cannot add.\n'.format(working_dir_raw))
            continue
        
        db = db_connect_exclusive(db_filename)
    
        if job_exists(db, working_dir):
            sys.stderr.write('A job at working directory {} already exists. Cannot add.\n'.format(working_dir))
        else:
            db.execute(
                'INSERT INTO jobs VALUES (?, ?, ?, ?, ?, ?, ?, ?);',
                [working_dir, exec_path, get_now_string(), 'waiting', None, None, None, None]
            )
            sys.stderr.write('Added job in working directory:\n  {}\nexecutable:\n  {}\n'.format(working_dir, exec_path))
    
        db_disconnect(db)
    
    if len(args.working_dir) == 0:
        sys.stderr.write('No jobs provided.\n')

def relative_path(path, file_relative, subpath_required = True):
    path = os.path.abspath(path)
    dir_relative = os.path.abspath(os.path.dirname(file_relative))
    relpath = os.path.relpath(path, dir_relative)
    if subpath_required and relpath.startswith(os.pardir):
        return None
    
    return relpath

def absolute_path(path, file_relative):
    return os.path.abspath(os.path.join(os.path.dirname(file_relative), path))

def is_subpath(path, root):
    if not path.endswith(os.sep):
        path = path + os.sep
    
    if not root.endswith(os.sep):
        root = root + os.sep
    return path.startswith(root)

def runr_remove(args):
    db_filename = identify_db_filename()
    if db_filename is None:
        sys.stderr.write('No runr database found. Cannot add.\n')
        sys.exit(1)
    
    if len(args.dir) == 0:
        sys.stderr.write('Remove all jobs in the current directory? Type yes to confirm:\n')
        result = raw_input('> ')
        if result.lower() == 'yes':
            root_dirs = ['.']
        else:
            sys.stderr.write('No jobs removed.\n')
            sys.exit(1)
    else:
        root_dirs = args.dir
    
    db = db_connect_exclusive(db_filename)
    
    removed_count = 0
    for dir in root_dirs:
        for root, dirs, files in os.walk(dir):
            working_dir = relative_path(root, db_filename)
            if job_exists(db, working_dir):
                state = get_job_state(db, working_dir)
                if state == 'running' and not args.force:
                    sys.stderr.write('Job still running in working directory:\n  {}\nCannot remove.\n'.format(working_dir))
                else:
                    db.execute('DELETE FROM jobs WHERE working_dir = ?;', [working_dir])
                    sys.stderr.write('Removed job in working directory:\n  {}\n'.format(working_dir))
                    removed_count += 1
    if removed_count == 0:
        sys.stderr.write('No jobs removed.\n')
    
    db_disconnect(db)

def runr_cancel(args):
    db_filename = identify_db_filename()
    if db_filename is None:
        sys.stderr.write('No runr database found. Cannot add.\n')
        sys.exit(1)
    
    if len(args.dir) == 0:
        sys.stderr.write('Cancel all jobs in the current directory? Type yes to confirm:\n')
        result = raw_input('> ')
        if result.lower() == 'yes':
            root_dirs = ['.']
        else:
            sys.stderr.write('No jobs canceled.\n')
            sys.exit(1)
    else:
        root_dirs = args.dir
    
    db = db_connect_exclusive(db_filename)
    removed_count = 0
    
    for dir in root_dirs:
        for root, dirs, files in os.walk(dir):
            working_dir = relative_path(root, db_filename)
            if job_exists(db, working_dir):
                state = get_job_state(db, working_dir)
                if state == 'running':
                    worker_id = get_worker_id(db, working_dir)
                    send_worker_message(db, worker_id, 'cancel', working_dir)
                    sys.stderr.write('Requested that worker {} cancel job in working directory:\n  {}\n'.format(worker_id, working_dir))
                    removed_count += 1
    if removed_count == 0:
        sys.stderr.write('No jobs canceled.\n')
    db_disconnect(db)

def runr_status(args, mode = 'status'):
    db_filename = identify_db_filename()
    if db_filename is None:
        sys.stderr.write('No runr database found. Cannot add.\n')
        sys.exit(1)
    
    if len(args.dir) == 0:
        root_dirs = ['.']
    else:
        root_dirs = args.dir
    
    db = db_connect_exclusive(db_filename)
    
    if args.state == 'all':
        all_job_info = [row for row in db.execute('SELECT * FROM jobs')]
    else:
        if not args.state in ['waiting', 'running', 'complete', 'failed', 'signaled', 'canceled']:
            sys.stderr.write('Invalid state {}; quitting.\n'.format(args.state))
            sys.exit(1)
        all_job_info = [row for row in db.execute('SELECT * FROM jobs WHERE state = ?', [args.state])]
    
    state_counter = Counter()
    total_time = datetime.timedelta()
    for root_dir in root_dirs:
        root_dir_absolute = os.path.abspath(root_dir)
        root_dir_relative = relative_path(root_dir_absolute, db_filename)
        for job_info in all_job_info:
            working_dir = job_info['working_dir']
            if is_subpath(absolute_path(working_dir, db_filename), root_dir_absolute):
                state_counter[job_info['state']] += 1
                
                if job_info['start_time'] is not None:
                    if job_info['end_time'] is not None:
                        elapsed_time = parse_date_string(job_info['end_time']) - parse_date_string(job_info['start_time'])
                    else:
                        elapsed_time = datetime.datetime.utcnow() - parse_date_string(job_info['start_time'])
                    total_time += elapsed_time
                else:
                    elapsed_time = None
                
                if mode == 'status':
                    sys.stderr.write('{}\n'.format(working_dir))
                    sys.stderr.write('{}'.format(job_info['state']))
                    if job_info['status_code'] is not None:
                        sys.stderr.write(' - status code {}\n'.format(job_info['status_code']))
                    else:
                        sys.stderr.write('\n')
                    sys.stderr.write('  creation time (UTC): {}\n'.format(job_info['creation_time']))
                    if job_info['start_time'] is not None:
                        sys.stderr.write('  start time (UTC): {}\n'.format(job_info['start_time']))
                    if job_info['end_time'] is not None:
                        sys.stderr.write('  end time (UTC): {}\n'.format(job_info['end_time']))
                    if elapsed_time is not None:
                        sys.stderr.write('  elapsed time: {}\n'.format(format_timedelta(elapsed_time)))
                
                    sys.stderr.write('\n')
                elif mode == 'list':
                    sys.stderr.write('{}\n'.format(working_dir))
    
    if mode == 'status' or mode == 'summary':
        job_count = sum(state_counter.values())
        sys.stderr.write('{} jobs total:\n'.format(job_count))
        if job_count > 0:
            for state in ['waiting', 'running', 'complete', 'failed', 'signaled', 'canceled']:
                sys.stderr.write('  {} {}\n'.format(state_counter[state], state))
            sys.stderr.write('Total elapsed time: {}\n'.format(format_timedelta(total_time)))
    
    db_disconnect(db)

def runr_reset(args):
    db_filename = identify_db_filename()
    
    if args.state == 'all':
        where_clause = 'state = "failed" OR state = "signaled" OR state = "canceled"'
    else:
        if not args.state in ['running', 'complete', 'failed', 'signaled', 'canceled']:
            sys.stderr.write('Invalid state {}; quitting.\n'.format(args.state))
            sys.exit(1)
        where_clause = 'state = "{}"'.format(args.state)
    
    db = db_connect_exclusive(db_filename)
    
    if not args.force:
        n_active_workers = db.execute('SELECT COUNT(*) FROM workers WHERE end_time IS NULL').next()[0]
        if n_active_workers > 0:
            sys.stderr.write('There are workers that have not terminated cleanly. If you know the workers died improperly, use --force to ignore them.\n')
            sys.exit(1)
    db.execute('UPDATE workers SET end_time = ? WHERE end_time IS NULL;', [get_now_string()])
    
    db.execute('UPDATE jobs SET state = "waiting", worker_id = NULL, start_time = NULL, end_time = NULL, status_code = NULL WHERE {}'.format(where_clause))
    db_disconnect(db)

def runr_go(args):
    db_filename = identify_db_filename()
    if db_filename is None:
        sys.stderr.write('No runr database found. Cannot go.\n')
        sys.exit(1)
    
    n_processes = args.processes
    
    db = db_connect_exclusive(db_filename)
    last_worker_id = db.execute('SELECT MAX(worker_id) FROM workers').next()[0]
    if last_worker_id is None:
        worker_id = 1
    else:
        worker_id = last_worker_id + 1
    db.execute('INSERT INTO workers VALUES (?,?,?,?);', [worker_id, n_processes, get_now_string(), None])
    db_disconnect(db)
    
    sys.stderr.write('Starting worker id {} with {} processes...\n'.format(worker_id, n_processes))
    
    done = False
    nothing_running_since = None
    
    job_queues = {}
    
    input_queue = Queue.Queue()
    
    # Spin off thread to check for control messages in the database
    receive_messages_queue = Queue.Queue()
    threading.Thread(
        target = receive_messages,
        args = (receive_messages_queue, input_queue, worker_id, db_filename)
    ).start()
    
    # Set up signal handler
    def runr_go_handle_signal(signum, frame):
        print 'SIGNAL HANDLER CALLED'
        if signum == signal.SIGKILL:
            print 'SIGKILL'
        elif signum == signal.SIGINT:
            print 'SIGINT'
        input_queue.put(['signal_message', signum])
    signal.signal(signal.SIGINT, runr_go_handle_signal)
    signal.signal(signal.SIGTERM, runr_go_handle_signal)
    
    signaled = False
    while (not done) or len(job_queues) > 0:
        # Start new jobs until we're running n_processes jobs or there aren't any more to run,
        # or we've been signaled or any job has been signaled
        while (not signaled) and (not done) and len(job_queues) < n_processes:
            db = db_connect_exclusive(db_filename)
            results = [row for row in db.execute(
                'SELECT working_dir, exec_path FROM jobs WHERE state = ? LIMIT 1',
                ['waiting']
            )]
            if len(results) == 0:
                done = True
                db_disconnect(db)
            else:
                assert len(results) == 1
                working_dir, exec_path = results[0]
                db.execute(
                    'UPDATE jobs SET state = ?, worker_id = ?, start_time = ? WHERE working_dir = ?',
                    ['running', worker_id, get_now_string(), working_dir]
                )
                db_disconnect(db)
                
                job_queues[working_dir] = Queue.Queue()
                
                threading.Thread(
                    target = run_job,
                    args = (job_queues[working_dir], input_queue, working_dir, exec_path, db_filename)
                ).start()
                sys.stderr.write('{} - Job started:\n  {}\n'.format(get_now_string(), working_dir))
        assert done or len(job_queues) == n_processes
        
        # Get one item from the input queue
        if len(job_queues) > 0:
            try:
                message_type, message_value = input_queue.get()
                
                if message_type == 'control_message':
                    command, working_dir = message_value
                    if command == 'cancel':
                        if working_dir in job_queues:
                            sys.stderr.write('Canceling job in working directory {}...\n'.format(working_dir))
                            job_queues[working_dir].put('cancel')
                elif message_type == 'completion_message':
                    working_dir, state, status_code = message_value
                    del job_queues[working_dir]
                    
                    # Update database with job state
                    db = db_connect_exclusive(db_filename)
                    db.execute(
                        'UPDATE jobs SET state = ?, end_time = ?, status_code = ? WHERE working_dir = ?',
                        [state, get_now_string(), status_code, working_dir]
                    )
                    db_disconnect(db)
                    
                    if state == 'signaled':
                        for working_dir in job_queues.iteritems():
                            job_queue.put('signal')
                        signaled = True
                        done = True
                    
                    sys.stderr.write('{} - Job {} (code {}):\n  {}\n'.format(get_now_string(), state, status_code, working_dir))
                elif message_type == 'signal_message':
                    print 'RECEIVED SIGNAL MESSAGE'
                    # Send message to job thread asking for cancellation
                    for working_dir, job_queue in job_queues.iteritems():
                       job_queue.put('signal')
                    signaled = True
                    done = True
            except:
                pass
    
    receive_messages_queue.put('terminate')
    
    db = db_connect_exclusive(db_filename)
    db.execute('UPDATE workers SET end_time = ? WHERE worker_id = ?;', [get_now_string(), worker_id])
    db_disconnect(db)
    
    sys.stderr.write('Worker exiting.\n')

def receive_messages(input_queue, output_queue, worker_id, db_filename):
    done = False
    while not done:
        time.sleep(random.expovariate(1.0 / 5.0))
        db = db_connect_exclusive(db_filename)
        messages = [row for row in db.execute(
            'SELECT command, working_dir FROM worker_messages WHERE worker_id = ?',
            [worker_id]
        )]
        if len(messages) > 0:
            db.execute('DELETE FROM worker_messages WHERE worker_id = ?', [worker_id])
        db_disconnect(db)
        
        for message in messages:
            output_queue.put(['control_message', message])
        
        try:
            while True:
                input_item = input_queue.get_nowait()
                assert input_item == 'terminate'
                done = True
        except:
            pass

def run_job(input_queue, output_queue, working_dir, exec_path, db_filename):
    exec_path_abs = absolute_path(exec_path, db_filename)
    working_dir_abs = absolute_path(working_dir, db_filename)
    
    stdout = open(os.path.join(working_dir_abs, 'stdout.txt'), 'w')
    proc = subprocess.Popen(
        exec_path_abs,
        cwd = working_dir_abs,
        stdout = stdout,
        stderr = subprocess.STDOUT
    )
    while proc.poll() is None:
        try:
            input_item = input_queue.get_nowait()
            assert input_item == 'cancel' or input_item == 'signal'
            
            if input_item == 'signal':
                print 'RECEIVED signal ON QUEUE; CANCELING JOB {}'.format(working_dir)
            
            state = 'canceled'
            status_code = None
            break
        except:
            pass
        time.sleep(1)
    stdout.close()
    
    if proc.returncode is None:
        assert state == 'canceled'
    else:
        status_code = proc.returncode
        if status_code == 0:
            state = 'complete'
        elif status_code > 0:
            state = 'failed'
        else:
            state = 'signaled'
            status_code = -status_code
    
    output_queue.put(['completion_message', (working_dir, state, status_code)])

def job_exists(db, working_dir):
    return db.execute(
        'SELECT COUNT(*) FROM jobs WHERE working_dir = ?;', [working_dir]
    ).next()[0] > 0

def get_job_state(db, working_dir):
    return db.execute(
        'SELECT state FROM jobs WHERE working_dir = ?', [working_dir]
    ).next()[0]

def get_job_info(db, working_dir):
    return db.execute(
        'SELECT * FROM jobs WHERE working_dir = ?', [working_dir]
    ).next()

def send_worker_message(db, worker_id, command, working_dir):
    db.execute(
        'INSERT INTO worker_messages VALUES (?,?,?,?)',
        [worker_id, get_now_string(), command, working_dir]
    )

def get_worker_id(db, working_dir):
    return db.execute(
        'SELECT worker_id FROM jobs WHERE working_dir = ?', [working_dir]
    ).next()[0]

def identify_db_filename():
    path = '.'
    while True:
        db_filename = os.path.join(path, DB_FILENAME)
        if os.path.exists(db_filename):
            return db_filename
        
        new_path = os.path.join(path, '..')
        if os.path.abspath(new_path) == os.path.abspath(path):
            break
        path = new_path
    return None

def detect_core_count():
    if sys.platform == 'darwin':
        return detect_core_count_darwin()
    raise Exception('unsupported platform')

def detect_core_count_darwin():
    return int(subprocess.check_output(['sysctl', '-n', 'hw.physicalcpu']))

def get_now_string():
    date = datetime.datetime.utcnow()
    return datetime.datetime(date.year, date.month, date.day, date.hour, date.minute, date.second).isoformat(' ')

def format_timedelta(td):
    days = td.days
    seconds = td.seconds
    hours = seconds // 3600
    seconds -= hours * 3600
    minutes = seconds // 60
    seconds -= minutes * 60
    
    return '{}d-{}h-{:02d}:{:02d}'.format(days, hours, minutes, seconds)

def parse_date_string(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    main()

