# runr: job runner

`runr` lets you run multiple jobs in parallel, either on a single computer or on multiple computers with a shared network filesystem.

In order to keep things simple, `runr` makes a few assumptions about your jobs:
* Each job runs in its own working directory. The working directory therefore acts as an identifier for the job.
* Each job can be run with a single executable that takes no arguments.
* Standard output and standard error can be mixed together and written to a single file named `stdout.txt` in the working directory.
* `runr` was implemented for use on the University of Chicago Midway cluster, so it should work there.
* Your jobs run on the same machine, or on multiple machines via a shared network filesystem that observes POSIX `fcntl()` locking semantics. This is a restrictive requirement: note, particularly, that NFS is unlikely to work reliably in many implementations. A recent, stable version of Samba, running exclusively with Unix clients, is likely to work. IBM's GPFS (used by U of C's Midway) and Red Hat's GFS2 should work.

`runr` otherwise knows nothing about your jobs and the directory hierarchy they live in. You are responsible for creating that directory hierarchy, either manually or by writing scripts.

## Setup

First, get a copy of `runr` via git:

```
cd wherever/you/want
git clone git@bitbucket.org:cobeylab/runr.git
```

You'll find it convenient to have `runr` in your search path. E.g., if you're using bash and `runr` is in `/project/cobey/runr`, add this to your `~/.bash_profile`:

```
export PATH=/project/cobey/runr:$PATH
```

## Running on one machine: an example

`runr` is best understood through a simple example. Say you've created directories named `job1`, `job2`, etc. inside a root directory named `experiment`, with files named `parameters.json` containing different parameters for each job, as well as a script `run.sh` that needs to run inside each of the `job*` directories:

```
experiment/
    job1/
        parameters.json
    job2/
        parameters.json
    ...
    run.sh
```

First, you use `runr init` to create a job queue:

```{sh}
cd experiment
runr init
```

Then, you add all the job directories to the job queue using `runr add`:

```{sh}
runr add run.sh job*
```

Finally, you run jobs in parallel, 4 at a time, using `runr go 4`:

```{sh}
runr go 4
```

That's it. The script `run.sh` will run 4 jobs in parallel. As soon as one job completes, the next one in the queue will take its place among the 4 parallel jobs.

More interestingly, if you run `runr go 4` on multiple machines with a shared file system, as soon as a job completes on *any* of the machines, it will pick up the next job to be run. For example, if you run `runr go 4` on 10 machines, there will always be 40 jobs running in parallel. Note that jobs are not assigned a priori to particular machines, so that no individual machine can run out of jobs until all of them have run out.

## Running on clusters

`runr` does not know anything about cluster management systems such as SLURM, but it can be layered very simply atop such a system.

If this seems redundant, these are the reasons for using it anyway:

* You can use essentially the same setup for running on your local machine as on a remote cluster.
* If your cluster imposes a policy that restricts you to, e.g., 48 simultaneous jobs running and 500 jobs queued, you can run 48 instances of `runr` and queue up as many jobs as you want.
* Even if your cluster allows you to include lots of sub-jobs ("job steps" in SLURM parlance) in your jobs, setting them up properly will be more complicated than using `runr`.
* Unlike a large job with many sub-jobs, individual instances of `runr` can get scheduled independently of each other, so if the cluster only has room for 10 of your 48 copies of `runr`, you can start running jobs immediately rather than waiting for the system to give you a huge allocation of processors.

Here's an example using SLURM. Let's return to our example from the introduction. Everything starts out the same:

```{sh}
cd experiment
runr init
runr add run.sh job*
```

Let's say you want to run on 48 nodes on your cluster, 8 processors per node, and each of your jobs uses 1 processor. You make a SLURM batch file with a 48-job array, specifying SLURM options as usual for each copy of `runr`:

```{sh}
#!/bin/bash
#SBATCH --array=1:48
#SBATCH --cpus-per-task=8
#SBATCH --time=36:00:00
#SBATCH --mem-per-cpu=4000
#SBATCH --output=%A-%a-stdout.txt
#SBATCH --error=%A-%a-stdout.txt
#SBATCH --signal=INT@300
srun runr go $SLURM_CPUS_PER_TASK
```

The last option: `--signal=INT@60` tells SLURM to send an interrupt signal to `runr` 5 minutes (300 seconds) before the official drop-dead time. This will ensure that `runr` has a chance to update its database to indicate that the jobs were canceled (rather than still being running).

SLURM will capture output from the `runr` program; individual jobs will pattern `%A-%a-stdout.txt` includes the identifier for the job array (`%A`) as well as the 

## Getting job status

Jobs can be in any of the following states:

* `waiting`: queued up; not yet run
* `running`: currently running
* `complete`: completed successfully (returned with status code 0)
* `failed`: completed unsuccessfully (returned with nonzero status code)
* `signaled`: job failed with signal
* `canceled`: job was canceled, either via `runr cancel` or due to the worker being interrupted

`runr status`: gives detailed information about all jobs in the current directory.

`runr summary`: gives a summary of how many jobs are in each state.

`runr list`: gives a machine-readable list of jobs.

All three commands can take a `--state` argument, which indicates which job state you want information for. For example,

```{sh}
runr list --state failed
```

will give a list of jobs that have failed, one on each line.


## Recovering from failure

If some jobs failed, or `runr go` ended prematurely, it's possible to indicate to `runr` that these jobs need to be re-run.

First, make sure that all instances of `runr go` have stopped.

First, do any cleanup you need to: e.g., use `runr list --state failed` to find all the jobs that failed, and write a script to reset their state if necessary.

Then, run

```{sh}
runr reset --state failed
```

to set the state of any `failed` jobs to `waiting`.

If `runr go` was stopped forcibly, e.g. due to a power outage or being forcibly killed, you'll need to use the `--force` option, which will indicate to the database that workers are no longer active:

```{sh}
runr reset --force --state failed
```

Finally, you can submit new `runr go` jobs, and the formerly `failed` jobs will be re-run.
